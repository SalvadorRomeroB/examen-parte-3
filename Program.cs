﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsertionSortGeneric
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = {2, 400, 334, 54, 4};

            InsertionGeneric.insertionSort<int>(arr, 5);
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine("{0 }", arr[i]);
            }
        }
    }
}
